# Assignemt 3 - RPG Character

Everything shuld be in order.

This program simulates RPG charracters: leveling up, using items (armor and weapons). The main method demonstrats this by creating some characters, leveling them up and giving them items.

There is however some issues - if this were to be a propper game. (If however you are a game investor looking for a primitive RPG game to develop - we can figure something out :-) )

# Issues:

If you dont un-equip an old item before equiping a new one the old one is deleted. This could be solved by adding a check that un-equips and return the old item.

There is also no way for the characters to accualy take damage (this was not in the task).

# Program Files:

Location: /src/main/java/

Main file: RPG_Hero.java

Packages: heroes and items (weapon and armor):

```
src/main/java -
                /heroes -
                    Hero.java
                    HeroSpecs.java
                    PrintHeroStats.java
                /items -
                    /armor -
                        Armor.java
                        ArmorType.java
                    /weapon -
                        Weapon.java
                        WeaponType.java
                RPGHero.java (main)
```

# Functionality:

The main method demonstrates how the program workes. All the characters have base-stats based on their class, and when they level up thay get a stat boost. The characters can also use 3 types of weapon, and 3 different armor sets (Head, body and leggs). All these items gives a boost of some sort.

By using the PrintHeroStats and PrintItemStats you can easely print all the relevant stats to the terminal for all items and all heroes.

# Implementation:

All information about the different items and heroes are stored in their respecteble emun file. This is also were most of the logic related to stats is stored.

The Hero class is the class with the most logic, this is because this is were the stats for different items is used by the hero.

I have also done alot of testing (see src/test). Most classes is implemented using TDD.

# Expansion:

It is easy to add items, hero classes and item types. All you need to change is located in the enum files.

If I for example was to add a ner hero class I only have to add a case in alle the switch statments in HeroSpecs.java


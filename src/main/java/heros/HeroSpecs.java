package heros;

/*
* All data related to each hero is stored here. In order to add a hero class you have to add the class name on top
* and add a 'case' for the new class in every method.
* */


public enum HeroSpecs {
    WARRIOR,
    RANGER,
    MAGE;

    public static String classToString(HeroSpecs h){
        switch (h){
            case WARRIOR -> {
                return "Warrior";
            }
            case RANGER -> {
                return "Ranger";
            }
            case MAGE -> {
                return "Mage";
            }
        }
        return "";

    }

    public static int health(HeroSpecs h){
        switch (h){
            case WARRIOR -> {
                return 150;
            }
            case RANGER -> {
                return 120;
            }
            case MAGE -> {
                return 100;
            }
            default -> {
                return 0;
            }
        }
    }
    public static int strength(HeroSpecs h){
        switch (h){
            case WARRIOR -> {
                return 10;
            }
            case RANGER -> {
                return 5;
            }
            case MAGE -> {
                return 2;
            }
            default -> {
                return 0;
            }
        }
    }
    public static int dexterity(HeroSpecs h){
        switch (h){
            case WARRIOR, MAGE -> {
                return 3;
            }
            case RANGER -> {
                return 10;
            }
            default -> {
                return 0;
            }
        }
    }
    public static int intelligence(HeroSpecs h){
        switch (h){
            case WARRIOR -> {
                return 1;
            }
            case RANGER -> {
                return 2;
            }
            case MAGE -> {
                return 10;
            }
            default -> {
                return 0;
            }
        }
    }

    public static int next_up_health(HeroSpecs h){
        switch (h){
            case WARRIOR -> {
                return 30;
            }
            case RANGER -> {
                return 20;
            }
            case MAGE -> {
                return 15;
            }
            default -> {
                return 0;
            }
        }
    }
    public static int next_up_strength(HeroSpecs h){
        switch (h){
            case WARRIOR -> {
                return 5;
            }
            case RANGER -> {
                return 2;
            }
            case MAGE -> {
                return 1;
            }
            default -> {
                return 0;
            }
        }
    }
    public static int next_up_dexterity(HeroSpecs h){
        switch (h){
            case WARRIOR, MAGE -> {
                return 2;
            }
            case RANGER -> {
                return 5;
            }
            default -> {
                return 0;
            }
        }
    }
    public static int next_up_intelligence(HeroSpecs h){
        switch (h){
            case WARRIOR, RANGER -> {
                return 1;
            }
            case MAGE -> {
                return 5;
            }
            default -> {
                return 0;
            }
        }
    }
}

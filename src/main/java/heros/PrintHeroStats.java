package heros;

import items.PrintItemStats;

public class PrintHeroStats {

    public static void printAllHeroStatsAllHeroes(Hero[] heroes){
        for (Hero h: heroes) {
            printAllStats(h);
        }
    }

    public static void printAllStats(Hero hero){

        System.out.println("\n -- " + hero.getName() + " -- ");
        System.out.println("Class: " + hero.getHeroClass());

        System.out.println("Level: " + hero.getLevel() + " (XP to next level: " + hero.getNeededExpToNextLevel() + ")");
        System.out.print("HP: " + hero.getHealth());
        System.out.print(" Str: " + hero.getStrength());
        System.out.print(" Int: " + hero.getIntelligence());
        System.out.print(" Dex: " + hero.getDexterity());
        System.out.println("\nItems Equipped:");

        PrintItemStats.printAllItemStats(hero.getItemSlotHead());
        PrintItemStats.printAllItemStats(hero.getItemSlotBody());
        PrintItemStats.printAllItemStats(hero.getItemSlotLegs());
        PrintItemStats.printAllItemStats(hero.getItemSlotWeapon());
    }
}

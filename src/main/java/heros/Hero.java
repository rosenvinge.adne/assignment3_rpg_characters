package heros;

import items.Item;
import items.Slot;
import items.armor.Armor;
import items.weapon.Weapon;

/*
* All logic related to the heroes
* Data is stored in HeroTypes.java
* */

public class Hero {
    HeroSpecs heroType;
    int health;
    int strength;
    int intelligence;
    int dexterity;

    int level = 1;

    int experience;
    int nextLevelRequirement = 100;

    protected String name;

    private Armor itemSlotHead, itemSlotBody, itemSlotLegs;
    private Weapon itemSlotWeapon;

    public Hero(String name, HeroSpecs type) {
        this.heroType = type;
        this.name = name;
        this.experience = 0;

        this.health = HeroSpecs.health(heroType);
        this.strength = HeroSpecs.strength(heroType);
        this.dexterity = HeroSpecs.dexterity(heroType);
        this.intelligence = HeroSpecs.intelligence(heroType);
    }

    // LEVEL UP
    private void levelUpdate(){
        while (experience >= nextLevelRequirement){
            levelUP();
        }
    }
    // Updates all stats and levels up the Hero.
    private void levelUP(){

        health += HeroSpecs.next_up_health(heroType);
        strength += HeroSpecs.next_up_strength(heroType);
        dexterity += HeroSpecs.next_up_dexterity(heroType);
        intelligence += HeroSpecs.next_up_intelligence(heroType);

        // Updates the next level exp requirements
        experience -= nextLevelRequirement;
        nextLevelRequirement = (int) (nextLevelRequirement * 1.1);

        level++;

    }
    // Hero gains exp
    public void gainExperience(int exp){
        experience += exp;
        levelUpdate();
    }
    // Deal damage based on what weapon is equipped
    public int dealDamage(){
        if (itemSlotWeapon == null){
            return 0;
        }else{
            switch (itemSlotWeapon.getWeaponType()){

                case MELEE -> {
                    return (int) (itemSlotWeapon.getDamage() + getStrength()*1.5);
                }
                case RANGE -> {
                    return itemSlotWeapon.getDamage() + getDexterity()*2;
                }
                case MAGIC -> {
                    return itemSlotWeapon.getDamage() + getIntelligence()*3;
                }
            }
        }
        return 0;

    }
    // Equips item to the right slot
    // This just over writes the current item, need to unEquip first if you want to keep the item
    public void equip(Item item){
        switch (item.getItemType()){
            case "Head" -> itemSlotHead = (Armor) item;
            case "Body" -> itemSlotBody = (Armor) item;
            case "Legs" -> itemSlotLegs = (Armor) item;
            case "Weapon" -> itemSlotWeapon = (Weapon) item;
        }
    }
    // returns the respectable item and returns it.
    public Item unEquip(Slot slot){
        Item i = null;
        switch (slot){
            case HEAD -> {
                i = itemSlotHead;
                itemSlotHead = null;
            }
            case BODY -> {
                i = itemSlotBody;
                itemSlotBody = null;
            }
            case LEGS -> {
                i = itemSlotLegs;
                itemSlotLegs = null;
            }
            case WEAPON -> {
                i = itemSlotWeapon;
                itemSlotWeapon = null;
            }
        }
        return i;
    }
    // Private methods to calculate the boosts gained from each item
    private int itemHealthBoost(){
        int boost = 0;
        for (Armor a: new Armor[]{itemSlotHead, itemSlotBody, itemSlotLegs}){
            if ( a!= null){
                boost += a.getHealthBonus();
            }
        }
        return boost;
    }
    private int itemStrengthBoost(){
        int boost = 0;
        for (Armor a: new Armor[]{itemSlotHead, itemSlotBody, itemSlotLegs}){
            if ( a!= null){
                boost += a.getStrengthBonus();
            }
        }
        return boost;
    }
    private int itemDexterityBoost(){
        int boost = 0;
        for (Armor a: new Armor[]{itemSlotHead, itemSlotBody, itemSlotLegs}){
            if ( a!= null){
                boost += a.getDexterityBonus();
            }
        }
        return boost;
    }
    private int itemIntelligenceBoost(){
        int boost = 0;
        for (Armor a: new Armor[]{itemSlotHead, itemSlotBody, itemSlotLegs}){
            if ( a!= null){
                boost += a.getIntelligenceBonus();
            }
        }
        return boost;
    }

    // GETTERS
    public String getHeroClass(){
        return HeroSpecs.classToString(heroType);
    }
    public int getNeededExpToNextLevel(){
        return nextLevelRequirement-experience;
    }
    public String getName(){
        return name;
    }
    public int getLevel() {
        return level;
    }

    // Uses the private methods "item....Boost()" to get the correct values
    public int getHealth() {
        return health + itemHealthBoost();
    }
    public int getStrength() {
        return strength + itemStrengthBoost();
    }
    public int getIntelligence() {
        return intelligence + itemIntelligenceBoost();
    }
    public int getDexterity() {
        return dexterity + itemDexterityBoost();
    }


    public Armor getItemSlotHead() {
        return itemSlotHead;
    }
    public Armor getItemSlotBody() {
        return itemSlotBody;
    }
    public Armor getItemSlotLegs() {
        return itemSlotLegs;
    }
    public Weapon getItemSlotWeapon() {
        return itemSlotWeapon;
    }
}

import heros.*;
import items.Slot;
import items.armor.Armor;
import items.armor.ArmorType;
import items.weapon.Weapon;
import items.weapon.WeaponType;


public class RPGHero {

    public static void main(String [] arg){

        // DEMONSTRATION:

        // CREATE HEROES
        Hero mage = new Hero("Gandalf", HeroSpecs.MAGE);
        Hero warrior = new Hero("Aragon", HeroSpecs.WARRIOR);
        Hero ranger = new Hero("Legolas", HeroSpecs.RANGER);

        Hero[] castOfLOTR = new Hero[]{mage, warrior, ranger};


        // Use PrintHeroStats to print the stats to terminal
        PrintHeroStats.printAllHeroStatsAllHeroes(castOfLOTR);

        // All gain different amount of exp
        mage.gainExperience(2000);
        warrior.gainExperience(210);
        ranger.gainExperience(1000);

        PrintHeroStats.printAllHeroStatsAllHeroes(castOfLOTR);

        // Everyone is given a weapon
        mage.equip(new Weapon("Laser pen", 1, WeaponType.MAGIC));
        warrior.equip(new Weapon("Really long stick", 1, WeaponType.RANGE));
        ranger.equip(new Weapon("Sowing needle", 1, WeaponType.MELEE));

        PrintHeroStats.printAllHeroStatsAllHeroes(castOfLOTR);


        // Demonstrate the damage dealt by all with - and without weapons
        for (Hero h:castOfLOTR) {
            System.out.println(h.getName() + " deals " + h.dealDamage() + " with " + h.getItemSlotWeapon().getName());
        }
        for (Hero h:castOfLOTR) {
            h.unEquip(Slot.WEAPON);
            System.out.println(h.getName() + " deals " + h.dealDamage() + " no weapon");
        }

        // ARMOR

        warrior.equip(new Armor("Tin Foil Hat", 10, ArmorType.PLATE, Slot.HEAD));
        warrior.equip(new Armor("Silck Slippers", 20, ArmorType.CLOTH, Slot.LEGS));

        mage.equip(new Armor("Cow Skin Hatt", 1, ArmorType.LEATHER, Slot.HEAD));
        ranger.equip(new Armor("Metal jacket", 3, ArmorType.PLATE, Slot.BODY));

        PrintHeroStats.printAllHeroStatsAllHeroes(castOfLOTR);
        warrior.unEquip(Slot.LEGS);
        mage.unEquip(Slot.HEAD);
        ranger.unEquip(Slot.BODY);
        PrintHeroStats.printAllHeroStatsAllHeroes(castOfLOTR);

    }

}

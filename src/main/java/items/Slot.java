package items;

/*
* Item slot information - Both armor and weapon have bore information and logic in their own file.
* */

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON;

    public static String itemSlotToString(Slot s){
        switch (s){
            case HEAD -> {
                return "Head";
            }
            case BODY -> {
                return "Body";
            }
            case LEGS -> {
                return "Legs";
            }
            case WEAPON -> {
                return "Weapon";
            }
        }
        return "";
    }
}

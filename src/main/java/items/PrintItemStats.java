package items;

import items.armor.Armor;
import items.weapon.Weapon;

public class PrintItemStats {

    /*
    * Prints all relevant stats related to every item.
    *
    * */


    public static void printAllItemStats(Item i){
        if (i!=null){

            System.out.println(" -- "+i.getName()+" --");
            System.out.println("Type: " + i.getItemType());
            System.out.println("Level: " + i.level);
            if (!i.getItemType().equals("Weapon")){
                printArmorStats((Armor) i);
            }else{
                printWeaponStats((Weapon) i);
            }
            System.out.println();

        }else{
            System.out.println(" - empty slot - ");
        }

    }
    private static void printArmorStats(Armor a){
        System.out.println("Type: " + a.getArmorType());
        System.out.print("Bonus:");
        System.out.println(
                " H: +" + a.getHealthBonus() +
                " S: +" + a.getStrengthBonus() +
                " D: +" + a.getDexterityBonus() +
                " I: +" + a.getIntelligenceBonus());

    }
    private static void printWeaponStats(Weapon w){
        System.out.println("Weapon Type: " + w.getWeaponType());
        System.out.println("Bonus damage: +" + w.getDamage());
    }
}

package items.armor;

/*
 * All data related to each Armor-type is stored here. In order to add a armor type you have to add the class name on top
 * and add a 'case' for the new type in every method.
 * */

public enum ArmorType {
    CLOTH, LEATHER, PLATE;

    public static int bonusHealth(ArmorType armorType, int level){
        switch (armorType){
            case CLOTH -> {
                return 10 + ((level-1) * 5);
            }
            case LEATHER -> {
                return 20 + ((level-1) * 8);
            }
            case PLATE -> {
                return 30 + ((level-1) * 12);
            }
        }
        return 0;
    }
    public static int bonusStrength(ArmorType armorType, int level){
        switch (armorType){
            case CLOTH -> {
                return 0;
            }
            case LEATHER -> {
                return 1 + level-1;
            }
            case PLATE -> {
                return 3 + ((level-1) * 2);
            }
        }
        return 0;
    }
    public static int bonusDexterity(ArmorType armorType, int level){
        switch (armorType){
            case CLOTH, PLATE -> {
                return 1 + level-1;
            }
            case LEATHER -> {
                return 3 + ((level-1) * 2);
            }
        }
        return 0;
    }
    public static int bonusIntelligence(ArmorType armorType, int level){
        if (armorType == ArmorType.CLOTH) {
            return 3 + ((level - 1) * 2);
        }
        return 0;
    }

    public static String armorTypeString(ArmorType armorType){
        switch (armorType){
            case CLOTH -> {
                return "Cloth";
            }
            case LEATHER -> {
                return "Leather";
            }
            case PLATE -> {
                return "Plate";
            }
        }
        return "";
    }

}

package items.armor;

import items.Item;
import items.Slot;

/*
* ALl Logic related to the Armor class
* */

public class Armor extends Item {
    ArmorType armorType;
    double effectiveness;

    public Armor(String name, int level, ArmorType armorType, Slot itemType) {
        super(name,level, itemType);
        this.armorType = armorType;
        setEffectiveness();
    }
    // Effectiveness is based on weather it is a head, body or leg piece
    private void setEffectiveness() {
        switch (itemType) {
            case HEAD -> effectiveness = 0.8;
            case BODY -> effectiveness = 1;
            case LEGS -> effectiveness = 0.6;
        }
    }
    // Getters
    public String getArmorType(){
        return ArmorType.armorTypeString(armorType);
    }

    // All bonus stats are calculated using the current effectiveness.
    public int getHealthBonus(){
        return (int) (ArmorType.bonusHealth(armorType, level) * effectiveness);
    }
    public int getStrengthBonus(){
        return (int) (ArmorType.bonusStrength(armorType, level) * effectiveness);
    }
    public int getDexterityBonus(){
        return (int) (ArmorType.bonusDexterity(armorType, level) * effectiveness);
    }
    public int getIntelligenceBonus(){
        return (int) (ArmorType.bonusIntelligence(armorType, level) * effectiveness);
    }
}

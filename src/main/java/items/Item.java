package items;

/*
* Parent class for all items
* The main logic is in WeaponType.java and ArmorType.java
*
* */

public abstract class Item {
    protected String name;
    protected int level;
    protected Slot itemType;


    protected Item(String name, int level, Slot itemType){
        this.name = name;
        this.itemType = itemType;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public String getItemType() {
        return Slot.itemSlotToString(itemType);
    }
}

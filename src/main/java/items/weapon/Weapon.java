package items.weapon;

import items.Item;
import items.Slot;

/*
* Weapon class
* All the logic is placed in WeaponType.java
* */

public class Weapon extends Item {
    WeaponType weaponType;

    public Weapon(String name, int level, WeaponType weaponType) {
        super(name,level, Slot.WEAPON);
        this.weaponType = weaponType;
    }

    public int getDamage() {
        return WeaponType.weaponBonus(weaponType, level);
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

}

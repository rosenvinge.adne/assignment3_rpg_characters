package items.weapon;

/*
 * All data related to each Weapon-type is stored here. In order to add a weapon type you have to add the class name on top
 * and add a 'case' for the new type in the weaponBonus() method.
 * */

public enum WeaponType {
    MELEE,
    RANGE,
    MAGIC;

    public static int weaponBonus(WeaponType weaponType, int level){
        switch (weaponType){

            case MELEE -> {
                return 15 + ((level-1) * 2);
            }
            case RANGE -> {
                return 5 + ((level-1) * 3);
            }
            case MAGIC -> {
                return 25 + ((level-1) * 2);
            }
        }
    return 0;
    }
}

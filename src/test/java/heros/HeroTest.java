package heros;

import items.Slot;
import items.weapon.Weapon;
import items.weapon.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/*
 * NOTE:
 *   This was my first go at TDD, and looking back I see I don't really follow the usual TDD pattern
 *   I still kept it like this. ArmorTest and WeaponTest is closer to what TDD usually look like.
 *
 * */

class WarriorTest {

    @Test
    void testWarrior(){
        Hero warrior = new Hero("warrior1", HeroSpecs.WARRIOR);

        assertEquals(warrior.getNeededExpToNextLevel(), 100);
        assertEquals(warrior.getLevel(), 1);

        assertEquals(warrior.getHealth(), 150);
        assertEquals(warrior.getStrength(), 10);
        assertEquals(warrior.getDexterity(), 3);
        assertEquals(warrior.getIntelligence(), 1);
    }

    @Test
    void testLevelUP(){

        Hero warrior1 = new Hero("warrior1", HeroSpecs.WARRIOR);

        // add one level at the time:

        warrior1.gainExperience(100);

        assertEquals(warrior1.getLevel(), 2);
        assertEquals(warrior1.getHealth(), 180);
        assertEquals(warrior1.getStrength(), 15);
        assertEquals(warrior1.getDexterity(), 5);
        assertEquals(warrior1.getIntelligence(), 2);

        // add more then one level:

        Hero warrior2 = new Hero("warrior2", HeroSpecs.WARRIOR);

        warrior2.gainExperience(300);

        assertEquals(warrior2.getLevel(), 3);
        assertEquals(warrior2.getHealth(), 210);
        assertEquals(warrior2.getStrength(), 20);
        assertEquals(warrior2.getDexterity(), 7);
        assertEquals(warrior2.getIntelligence(), 3);
    }

    @Test
    void testDealDamage(){
        Hero warrior = new Hero("warrior", HeroSpecs.WARRIOR);

        // NO ITEMS:

        assertEquals(warrior.dealDamage(), 0);

        // WITH ITEM:

        //TODO:
    }

}

class RangerTest {

    @Test
    void testRanger(){
        Hero ranger = new Hero("ranger1", HeroSpecs.RANGER);

        assertEquals(ranger.getNeededExpToNextLevel(), 100);
        assertEquals(ranger.getLevel(), 1);

        assertEquals(ranger.getHealth(), 120);
        assertEquals(ranger.getStrength(), 5);
        assertEquals(ranger.getDexterity(), 10);
        assertEquals(ranger.getIntelligence(), 2);
    }

    @Test
    void testLevelUP(){

        Hero ranger1 = new Hero("ranger1", HeroSpecs.RANGER);

        // add one level at the time:

        ranger1.gainExperience(100);

        assertEquals(ranger1.getLevel(), 2);
        assertEquals(ranger1.getHealth(), 140);
        assertEquals(ranger1.getStrength(), 7);
        assertEquals(ranger1.getDexterity(), 15);
        assertEquals(ranger1.getIntelligence(), 3);

        // add more then one level:

        Hero ranger2 = new Hero("ranger2", HeroSpecs.RANGER);

        ranger2.gainExperience(300);

        assertEquals(ranger2.getLevel(), 3);
        assertEquals(ranger2.getHealth(), 160);
        assertEquals(ranger2.getStrength(), 9);
        assertEquals(ranger2.getDexterity(), 20);
        assertEquals(ranger2.getIntelligence(), 4);
    }

}

class MageTest {

    @Test
    void testMage(){
        Hero mage = new Hero("mage1", HeroSpecs.MAGE);

        assertEquals(mage.getNeededExpToNextLevel(), 100);
        assertEquals(mage.getLevel(), 1);

        assertEquals(mage.getHealth(), 100);
        assertEquals(mage.getStrength(), 2);
        assertEquals(mage.getDexterity(), 3);
        assertEquals(mage.getIntelligence(), 10);
    }

    @Test
    void testLevelUP(){

        Hero mage1 = new Hero("mage1", HeroSpecs.MAGE);

        // add one level at the time:

        mage1.gainExperience(100);

        assertEquals(mage1.getLevel(), 2);
        assertEquals(mage1.getHealth(), 115);
        assertEquals(mage1.getStrength(), 3);
        assertEquals(mage1.getDexterity(), 5);
        assertEquals(mage1.getIntelligence(), 15);

        // add more then one level:

        Hero mage2 = new Hero("mage2", HeroSpecs.MAGE);

        mage2.gainExperience(300);

        assertEquals(mage2.getLevel(), 3);
        assertEquals(mage2.getHealth(), 130);
        assertEquals(mage2.getStrength(), 4);
        assertEquals(mage2.getDexterity(), 7);
        assertEquals(mage2.getIntelligence(), 20);
    }

    @Test
    void testDealDamage(){
        Hero mage = new Hero("mage", HeroSpecs.MAGE);

        // NO ITEMS:

        assertEquals(mage.dealDamage(), 0);

        // WITH ITEM:

        // MELEE:

        mage.equip(new Weapon("melee", 1, WeaponType.MELEE));
        assertEquals(mage.dealDamage(), (int) (mage.getItemSlotWeapon().getDamage() + mage.getStrength()*1.5));
        mage.equip(new Weapon("melee", 4, WeaponType.MELEE));
        assertEquals(mage.dealDamage(), (int) (mage.getItemSlotWeapon().getDamage() + mage.getStrength()*1.5));

        // RANGE:

        mage.equip(new Weapon("melee", 1, WeaponType.RANGE));
        assertEquals(mage.dealDamage(), (int) (mage.getItemSlotWeapon().getDamage() + mage.getDexterity()*2));
        mage.equip(new Weapon("melee", 4, WeaponType.RANGE));
        assertEquals(mage.dealDamage(), (int) (mage.getItemSlotWeapon().getDamage() + mage.getDexterity()*2));

        // MAGIC:

        mage.equip(new Weapon("melee", 1, WeaponType.MAGIC));
        assertEquals(mage.dealDamage(), (int) (mage.getItemSlotWeapon().getDamage() + mage.getIntelligence()*3));
        mage.equip(new Weapon("melee", 4, WeaponType.MAGIC));
        assertEquals(mage.dealDamage(), (int) (mage.getItemSlotWeapon().getDamage() + mage.getIntelligence()*3));

        // UNEPUIP:

        mage.unEquip(Slot.WEAPON);
        assertEquals(mage.dealDamage(), 0);

    }

}
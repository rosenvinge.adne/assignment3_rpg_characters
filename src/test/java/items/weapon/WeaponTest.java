package items.weapon;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {
    /*
     * Armor and Weapon were implemented as close to TDD ass I could manage. I test for all the corner cases I could think of
     * */

    @Test
    void getDamage() {
        Weapon meleeLevelOne = new Weapon("mw1", 1, WeaponType.MELEE);
        Weapon meleeLevelTwo = new Weapon("mw2", 2, WeaponType.MELEE);

        assertEquals(meleeLevelOne.getDamage(), 15);
        assertEquals(meleeLevelTwo.getDamage(), 17);

        Weapon rangedLevelOne = new Weapon("rw1", 1, WeaponType.RANGE);
        Weapon rangedLevelTwo = new Weapon("rw2", 2, WeaponType.RANGE);

        assertEquals(rangedLevelOne.getDamage(), 5);
        assertEquals(rangedLevelTwo.getDamage(), 8);

        Weapon magicLevelOne = new Weapon("mw1", 1, WeaponType.MAGIC);
        Weapon magicLevelTwo = new Weapon("mw2", 2, WeaponType.MAGIC);

        assertEquals(magicLevelOne.getDamage(), 25);
        assertEquals(magicLevelTwo.getDamage(), 27);
    }

    @Test
    void getWeaponType() {
        Weapon melee = new Weapon("mw1", 1, WeaponType.MELEE);

        assertEquals(melee.getWeaponType(), WeaponType.MELEE);

        Weapon ranged = new Weapon("rw1", 1, WeaponType.RANGE);
        assertEquals(ranged.getWeaponType(), WeaponType.RANGE);

        Weapon magic = new Weapon("mw1", 1, WeaponType.MAGIC);
        assertEquals(magic.getWeaponType(), WeaponType.MAGIC);
    }
}
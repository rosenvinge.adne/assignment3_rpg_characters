package items.armor;

import items.Slot;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {
    /*
    * Armor and Weapon were implemented as close to TDD ass I could manage. I test for all the corner cases I could think of
    * */
    @Test
    void getHealthBonus() {

        //CLOTH ARMOR

        Armor clothHelmet = new Armor("c", 1, ArmorType.CLOTH, Slot.HEAD);
        assertEquals(clothHelmet.getHealthBonus(), (int) (10*0.8));
        Armor clothHelmet2 = new Armor("c", 2,ArmorType.CLOTH, Slot.HEAD);
        assertEquals(clothHelmet2.getHealthBonus(), (int) (15*0.8));

        Armor clothBody = new Armor("c", 1,ArmorType.CLOTH, Slot.BODY);
        assertEquals(clothBody.getHealthBonus(), (int) (10));
        Armor clothBody2 = new Armor("c", 2,ArmorType.CLOTH, Slot.BODY);
        assertEquals(clothBody2.getHealthBonus(), (int) (15));

        Armor clothBot = new Armor("c", 1,ArmorType.CLOTH, Slot.LEGS);
        assertEquals(clothBot.getHealthBonus(), (int) (10*0.6));
        Armor clothBoot2 = new Armor("c", 2,ArmorType.CLOTH, Slot.LEGS);
        assertEquals(clothBoot2.getHealthBonus(), (int) (15*0.6));

        //LEATHER ARMOR

        Armor leatherHelmet = new Armor("c", 1,ArmorType.LEATHER, Slot.HEAD);
        assertEquals(leatherHelmet.getHealthBonus(), (int) (20*0.8));
        Armor leatherHelmet2 = new Armor("c", 2,ArmorType.LEATHER, Slot.HEAD);
        assertEquals(leatherHelmet2.getHealthBonus(), (int) (28*0.8));

        Armor leatherBody = new Armor("c", 1,ArmorType.LEATHER, Slot.BODY);
        assertEquals(leatherBody.getHealthBonus(), (int) (20));
        Armor leatherBody2 = new Armor("c", 2,ArmorType.LEATHER, Slot.BODY);
        assertEquals(leatherBody2.getHealthBonus(), (int) (28));

        Armor leatherBoot = new Armor("c", 1,ArmorType.LEATHER, Slot.LEGS);
        assertEquals(leatherBoot.getHealthBonus(), (int) (20*0.6));
        Armor leatherBoot2 = new Armor("c", 2,ArmorType.LEATHER, Slot.LEGS);
        assertEquals(leatherBoot2.getHealthBonus(), (int) (28*0.6));

        //PLATE ARMOR

        Armor plateHelmet = new Armor("c", 1,ArmorType.PLATE, Slot.HEAD);
        assertEquals(plateHelmet.getHealthBonus(), (int) (30*0.8));
        Armor plateHelmet2 = new Armor("c", 2,ArmorType.PLATE, Slot.HEAD);
        assertEquals(plateHelmet2.getHealthBonus(), (int) (42*0.8));

        Armor plateBody = new Armor("c", 1,ArmorType.PLATE, Slot.BODY);
        assertEquals(plateBody.getHealthBonus(), (int) (30));
        Armor plateBody2 = new Armor("c", 2,ArmorType.PLATE, Slot.BODY);
        assertEquals(plateBody2.getHealthBonus(), (int) (42));

        Armor plateBoot = new Armor("c", 1,ArmorType.PLATE, Slot.LEGS);
        assertEquals(plateBoot.getHealthBonus(), (int) (30*0.6));
        Armor plateBoot2 = new Armor("c", 2,ArmorType.PLATE, Slot.LEGS);
        assertEquals(plateBoot2.getHealthBonus(), (int) (42*0.6));

    }

    @Test
    void getStrengthBonus() {

        //CLOTH ARMOR

        Armor clothHelmet = new Armor("c", 1,ArmorType.CLOTH, Slot.HEAD);
        assertEquals(clothHelmet.getStrengthBonus(), 0);
        Armor clothHelmet2 = new Armor("c", 2,ArmorType.CLOTH, Slot.HEAD);
        assertEquals(clothHelmet2.getStrengthBonus(), 0);

        Armor clothBody = new Armor("c", 1,ArmorType.CLOTH, Slot.BODY);
        assertEquals(clothBody.getStrengthBonus(), 0);
        Armor clothBody2 = new Armor("c", 2,ArmorType.CLOTH, Slot.BODY);
        assertEquals(clothBody2.getStrengthBonus(), 0);

        Armor clothBot = new Armor("c", 1,ArmorType.CLOTH, Slot.LEGS);
        assertEquals(clothBot.getStrengthBonus(), 0);
        Armor clothBoot2 = new Armor("c", 2,ArmorType.CLOTH, Slot.LEGS);
        assertEquals(clothBoot2.getStrengthBonus(), 0);

        //LEATHER ARMOR

        Armor leatherHelmet = new Armor("c", 1,ArmorType.LEATHER, Slot.HEAD);
        assertEquals(leatherHelmet.getStrengthBonus(), (int) (1*0.8));
        Armor leatherHelmet2 = new Armor("c", 2,ArmorType.LEATHER, Slot.HEAD);
        assertEquals(leatherHelmet2.getStrengthBonus(), (int) (2*0.8));

        Armor leatherBody = new Armor("c", 1,ArmorType.LEATHER, Slot.BODY);
        assertEquals(leatherBody.getStrengthBonus(), (int) (1));
        Armor leatherBody2 = new Armor("c", 2,ArmorType.LEATHER, Slot.BODY);
        assertEquals(leatherBody2.getStrengthBonus(), (int) (2));

        Armor leatherBoot = new Armor("c", 1,ArmorType.LEATHER, Slot.LEGS);
        assertEquals(leatherBoot.getStrengthBonus(), (int) (1*0.6));
        Armor leatherBoot2 = new Armor("c", 2,ArmorType.LEATHER, Slot.LEGS);
        assertEquals(leatherBoot2.getStrengthBonus(), (int) (2*0.6));

        //PLATE ARMOR

        Armor plateHelmet = new Armor("c", 1,ArmorType.PLATE, Slot.HEAD);
        assertEquals(plateHelmet.getStrengthBonus(), (int) (3*0.8));
        Armor plateHelmet2 = new Armor("c", 2,ArmorType.PLATE, Slot.HEAD);
        assertEquals(plateHelmet2.getStrengthBonus(), (int) (5*0.8));

        Armor plateBody = new Armor("c", 1,ArmorType.PLATE, Slot.BODY);
        assertEquals(plateBody.getStrengthBonus(), (int) (3));
        Armor plateBody2 = new Armor("c", 2,ArmorType.PLATE, Slot.BODY);
        assertEquals(plateBody2.getStrengthBonus(), (int) (5));

        Armor plateBoot = new Armor("c", 1,ArmorType.PLATE, Slot.LEGS);
        assertEquals(plateBoot.getStrengthBonus(), (int) (3*0.6));
        Armor plateBoot2 = new Armor("c", 2,ArmorType.PLATE, Slot.LEGS);
        assertEquals(plateBoot2.getStrengthBonus(), (int) (5*0.6));
    }

    @Test
    void getDexterityBonus() {

        //CLOTH ARMOR

        Armor clothHelmet = new Armor("c", 1,ArmorType.CLOTH, Slot.HEAD);
        assertEquals(clothHelmet.getDexterityBonus(), (int) (1 * 0.8));
        Armor clothHelmet2 = new Armor("c", 2,ArmorType.CLOTH, Slot.HEAD);
        assertEquals(clothHelmet2.getDexterityBonus(), (int) (2 * 0.8));

        Armor clothBody = new Armor("c", 1,ArmorType.CLOTH, Slot.BODY);
        assertEquals(clothBody.getDexterityBonus(), 1);
        Armor clothBody2 = new Armor("c", 2,ArmorType.CLOTH, Slot.BODY);
        assertEquals(clothBody2.getDexterityBonus(), 2);

        Armor clothBot = new Armor("c", 1,ArmorType.CLOTH, Slot.LEGS);
        assertEquals(clothBot.getDexterityBonus(), (int)(1*0.6));
        Armor clothBoot2 = new Armor("c", 2,ArmorType.CLOTH, Slot.LEGS);
        assertEquals(clothBoot2.getDexterityBonus(), (int)(2*0.6));

        //LEATHER ARMOR

        Armor leatherHelmet = new Armor("c", 1,ArmorType.LEATHER, Slot.HEAD);
        assertEquals(leatherHelmet.getDexterityBonus(), (int) (3*0.8));
        Armor leatherHelmet2 = new Armor("c", 2,ArmorType.LEATHER, Slot.HEAD);
        assertEquals(leatherHelmet2.getDexterityBonus(), (int) (5*0.8));

        Armor leatherBody = new Armor("c", 1,ArmorType.LEATHER, Slot.BODY);
        assertEquals(leatherBody.getDexterityBonus(), (int) (3));
        Armor leatherBody2 = new Armor("c", 2,ArmorType.LEATHER, Slot.BODY);
        assertEquals(leatherBody2.getDexterityBonus(), (int) (5));

        Armor leatherBoot = new Armor("c", 1,ArmorType.LEATHER, Slot.LEGS);
        assertEquals(leatherBoot.getDexterityBonus(), (int) (3*0.6));
        Armor leatherBoot2 = new Armor("c", 2,ArmorType.LEATHER, Slot.LEGS);
        assertEquals(leatherBoot2.getDexterityBonus(), (int) (5*0.6));

        //PLATE ARMOR

        Armor plateHelmet = new Armor("c", 1,ArmorType.PLATE, Slot.HEAD);
        assertEquals(plateHelmet.getDexterityBonus(), (int) (1*0.8));
        Armor plateHelmet2 = new Armor("c", 2,ArmorType.PLATE, Slot.HEAD);
        assertEquals(plateHelmet2.getDexterityBonus(), (int) (2*0.8));

        Armor plateBody = new Armor("c", 1,ArmorType.PLATE, Slot.BODY);
        assertEquals(plateBody.getDexterityBonus(), (int) (1));
        Armor plateBody2 = new Armor("c", 2,ArmorType.PLATE, Slot.BODY);
        assertEquals(plateBody2.getDexterityBonus(), (int) (2));

        Armor plateBoot = new Armor("c", 1,ArmorType.PLATE, Slot.LEGS);
        assertEquals(plateBoot.getDexterityBonus(), (int) (1*0.6));
        Armor plateBoot2 = new Armor("c", 2,ArmorType.PLATE, Slot.LEGS);
        assertEquals(plateBoot2.getDexterityBonus(), (int) (2*0.6));
    }

    @Test
    void getIntelligenceBonus() {

        //CLOTH ARMOR

        Armor clothHelmet = new Armor("c", 1,ArmorType.CLOTH, Slot.HEAD);
        assertEquals(clothHelmet.getIntelligenceBonus(), (int) (3 * 0.8));
        Armor clothHelmet2 = new Armor("c", 2,ArmorType.CLOTH, Slot.HEAD);
        assertEquals(clothHelmet2.getIntelligenceBonus(), (int) (5 * 0.8));

        Armor clothBody = new Armor("c", 1,ArmorType.CLOTH, Slot.BODY);
        assertEquals(clothBody.getIntelligenceBonus(), 3);
        Armor clothBody2 = new Armor("c", 2,ArmorType.CLOTH, Slot.BODY);
        assertEquals(clothBody2.getIntelligenceBonus(), 5);

        Armor clothBot = new Armor("c", 1,ArmorType.CLOTH, Slot.LEGS);
        assertEquals(clothBot.getIntelligenceBonus(), (int)(3*0.6));
        Armor clothBoot2 = new Armor("c", 2,ArmorType.CLOTH, Slot.LEGS);
        assertEquals(clothBoot2.getIntelligenceBonus(), (int)(5*0.6));

        //LEATHER ARMOR

        Armor leatherHelmet = new Armor("c", 1,ArmorType.LEATHER, Slot.HEAD);
        assertEquals(leatherHelmet.getIntelligenceBonus(), (int) (0));
        Armor leatherHelmet2 = new Armor("c", 2,ArmorType.LEATHER, Slot.HEAD);
        assertEquals(leatherHelmet2.getIntelligenceBonus(), (int) (0));

        Armor leatherBody = new Armor("c", 1,ArmorType.LEATHER, Slot.BODY);
        assertEquals(leatherBody.getIntelligenceBonus(), (int) (0));
        Armor leatherBody2 = new Armor("c", 2,ArmorType.LEATHER, Slot.BODY);
        assertEquals(leatherBody2.getIntelligenceBonus(), (int) (0));

        Armor leatherBoot = new Armor("c", 1,ArmorType.LEATHER, Slot.LEGS);
        assertEquals(leatherBoot.getIntelligenceBonus(), (int) (0));
        Armor leatherBoot2 = new Armor("c", 2,ArmorType.LEATHER, Slot.LEGS);
        assertEquals(leatherBoot2.getIntelligenceBonus(), (int) (0));

        //PLATE ARMOR

        Armor plateHelmet = new Armor("c", 1,ArmorType.PLATE, Slot.HEAD);
        assertEquals(plateHelmet.getIntelligenceBonus(), (int) (0*0.8));
        Armor plateHelmet2 = new Armor("c", 2,ArmorType.PLATE, Slot.HEAD);
        assertEquals(plateHelmet2.getIntelligenceBonus(), (int) (0*0.8));

        Armor plateBody = new Armor("c", 1,ArmorType.PLATE, Slot.BODY);
        assertEquals(plateBody.getIntelligenceBonus(), (int) (0));
        Armor plateBody2 = new Armor("c", 2,ArmorType.PLATE, Slot.BODY);
        assertEquals(plateBody2.getIntelligenceBonus(), (int) (0));

        Armor plateBoot = new Armor("c", 1,ArmorType.PLATE, Slot.LEGS);
        assertEquals(plateBoot.getIntelligenceBonus(), (int) (0*0.6));
        Armor plateBoot2 = new Armor("c", 2,ArmorType.PLATE, Slot.LEGS);
        assertEquals(plateBoot2.getIntelligenceBonus(), (int) (0*0.6));
    }
}